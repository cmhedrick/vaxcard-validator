from django.apps import AppConfig


class VaxvalidatorConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'vaxvalidator'
