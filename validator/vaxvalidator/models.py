from django.db import models


class VaccineAbstract(models.Model):
    lot_num = models.CharField(max_length=32, unique=True)
    expiration_date = models.DateField()
    country_code = models.CharField(max_length=3)
    previous_expiration_date = models.TextField(blank=True)


class ModernaVaccine(VaccineAbstract):
    @property
    def sytem_name(self):
        return f"Moderna: LOT# {self.lot_num}"

    def __str__(self):
        return self.sytem_name


class JanssenVaccine(VaccineAbstract):
    product_name = models.CharField(max_length=32)
    last_updated_on = models.DateTimeField()
    material = models.CharField(max_length=32)
    sku_description = models.TextField()
    batch_status = models.CharField(max_length=32)
    unrestricted = models.CharField(max_length=32)
    restricted = models.CharField(max_length=32)
    quality_inspection = models.CharField(max_length=32)
    z_enterprise_material_number = models.CharField(max_length=32)
    is_negative_temp = models.BooleanField()
    time_stamp = models.DateTimeField()

    @property
    def sytem_name(self):
        return f"J&J: LOT# {self.lot_num}"

    def __str__(self):
        return self.sytem_name
