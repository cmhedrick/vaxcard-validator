import json
from typing import List, Dict
from datetime import datetime

import requests
from django.conf import settings
from django.utils import timezone
from django.utils.timezone import get_current_timezone
from django.core.management.base import BaseCommand, CommandError
from simple_pushover import PushoverAPI

from vaxvalidator.models import ModernaVaccine

local_timezone = get_current_timezone()

push_api = PushoverAPI()


class Command(BaseCommand):
    help = "Fetches the expiry.json from moderna website."

    def add_arguments(self, parser):
        # Named (optional) arguments
        parser.add_argument(
            "--save-json",
            action="store_true",
            help="Save JSON file locally.",
        )

    def get_lot_json(self) -> List[Dict[str, str]]:
        user_agent = (
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36"
            + " (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36"
        )
        # url to send requst to and to set up as referer
        url = settings.MODERNA_URL
        # referer needed to get past weak COR Policy??
        headers = {
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "en-US,en;q=0.9",
            "User-Agent": user_agent,
            "Accept": "application/json, text/plain, */*",
            "Referer": url,
        }

        req = requests.get(url, headers=headers)
        return req.json()

    def save_json_local(self, json_list: List[Dict[str, str]]) -> None:
        with open("moderna_expiry.json", "w") as outfile:
            json.dump(json_list, outfile)

    def parse_date(self, date_str: str):
        parsed_date = local_timezone.localize(
            datetime.strptime(date_str, "%Y-%m-%d")
        ).date()
        return parsed_date

    def handle(self, *args, **options):
        created_count = 0
        updated_count = 0
        try:
            moderna_json = self.get_lot_json()
        except Exception as e:
            self.stdout.write(
                self.style.ERROR(f"Failed to retrieve expiry.json! Explain: {e}")
            )
            raise

        for batch in moderna_json:
            # break this out because complicated
            vax = ModernaVaccine.objects.filter(lot_num=batch.get("Batch")).first()
            if vax:
                expiration_date = self.parse_date(batch.get("ExpiryDate"))
                if vax.expiration_date != expiration_date:
                    # take old date and log it
                    vax.previous_expiration_date = vax.previous_expiration_date = (
                        vax.expiration_date.strftime("%m/%d/%Y")
                        + "\n"
                        + vax.previous_expiration_date
                    )
                    # set the new expiration
                    vax.expiration_date = self.parse_date(batch.get("ExpiryDate"))
                    updated_count += 1
            else:
                vax = ModernaVaccine.objects.create(
                    lot_num=batch.get("Batch"),
                    expiration_date=self.parse_date(batch.get("ExpiryDate")),
                    country_code="US",
                )
                created_count += 1
            # save results
            vax.save()
        msg = ""
        if created_count > 0:
            msg += f"Created {created_count} new Moderna record(s)!"

        if updated_count > 0:
            if msg:
                msg += "\n"

            msg += f"Updated {updated_count} new Moderna record(s)!"

        if msg:
            self.stdout.write(self.style.SUCCESS(msg))
            push_api.send_push(message=msg)

        if created_count == 0 and updated_count == 0:
            self.stdout.write(self.style.SUCCESS(f"No new Moderna records!"))
            push_api.send_push(message=f"No new Moderna records!")

        if options["save_json"]:
            self.save_json_local(moderna_json)
            self.stdout.write(self.style.SUCCESS(f"Expiry.json saved locally!"))
