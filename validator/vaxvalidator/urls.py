from django.urls import path

from . import views

urlpatterns = [
    # path('', views.HomeView.as_view(), name='index'),
    path('', views.LookupLotView.as_view(), name='lot_lookup'),
    path('results/<str:vaccine_brand>/<str:dose_1_lot>/',
         views.ResultsView.as_view(), name='results'),
    path('results/<str:vaccine_brand>/<str:dose_1_lot>/<str:dose_2_lot>',
         views.ResultsView.as_view(), name='results'),
    path('privacy-policy', views.PrivacyPolicyView.as_view(), name='privacy-policy'),
]
