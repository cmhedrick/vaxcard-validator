from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponse
from django.conf import settings
from django.views.generic import TemplateView, FormView


from .forms import LotLookUpForm
from vaxvalidator.models import ModernaVaccine, JanssenVaccine
from vaxvalidator.utils import get_and_create_jnj


class PrivacyPolicyView(TemplateView):
    template_name = "privacy-policy.html"


class ResultsView(TemplateView):
    template_name = "results.html"

    def get_context_data(self, **kwargs):
        context = super(ResultsView, self).get_context_data(**kwargs)
        clean_path = self.request.path.strip("/").split("/")
        context["vaccine_brand"] = clean_path[1]
        context["lot_num_1"] = clean_path[2]
        if len(clean_path) > 3:
            context["lot_num_2"] = clean_path[3]
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        context["lot_num_1_result"] = self.fetch_lot_data(
            context["vaccine_brand"], context["lot_num_1"]
        )
        if context.get("lot_num_2"):
            context["lot_num_2_result"] = self.fetch_lot_data(
                context["vaccine_brand"], context["lot_num_2"]
            )
        return render(request, self.template_name, context=context)

    def fetch_lot_data(self, vaccine_brand, lot_num):
        if vaccine_brand == "moderna":
            result = self.validate_moderna(lot_num)
        elif vaccine_brand == "janssen":
            result = self.validate_janssen(lot_num)
        return result

    def validate_moderna(self, lot_num):
        """
        Moderna is always stored locally so we can retrieve it from the db.
        """
        return ModernaVaccine.objects.filter(lot_num=lot_num).exists()

    def validate_janssen(self, lot_num):
        janssen_vax = JanssenVaccine.objects.filter(lot_num=lot_num).exists()
        if janssen_vax:
            return True
        if not janssen_vax:
            janssen_vax, created = get_and_create_jnj(lot_num)
            if created:
                return True
        return False


class LookupLotView(FormView):
    template_name = 'lot_lookup_form.html'
    form_class = LotLookUpForm
    success_url = '/results'

    def get_context_data(self, **kwargs):
        context = super(LookupLotView, self).get_context_data(**kwargs)
        context["hcaptcha_key"] = settings.HCAPTCHA_SITEKEY
        return context

    def form_valid(self, form):
        # return super().form_valid(form)
        vaccine_brand = form.cleaned_data.get("vaccine_brand")
        lot_num_1 = form.cleaned_data.get("lot_num_1")
        lot_num_2 = form.cleaned_data.get("lot_num_2", "")
        return redirect(f"results/{vaccine_brand}/{lot_num_1}/{lot_num_2}")
