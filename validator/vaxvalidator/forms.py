import requests
from django import forms
from django.conf import settings
from django.core.exceptions import ValidationError

from vaxvalidator.models import ModernaVaccine, JanssenVaccine
from vaxvalidator.utils import get_and_create_jnj


class LotLookUpForm(forms.Form):
    VACCINE_CHOICES = [
        ('moderna', 'Moderna'),
        ('janssen', 'J&J/Janssen'),
    ]
    vaccine_brand = forms.CharField(
        label='Vaccine Brand', widget=forms.Select(choices=VACCINE_CHOICES)
    )
    lot_num_1 = forms.CharField(label='First Dose LOT#')
    lot_num_2 = forms.CharField(label='Second Dose LOT#', required=False)

    def clean(self):
        cleaned_data = super().clean()
        lot_num_1 = cleaned_data.get("lot_num_1")
        lot_num_2 = cleaned_data.get("lot_num_2")
        h_resp = self.data.get("h-captcha-response", "")

        if not h_resp:
            raise ValidationError(
                "Please complete the CAPTCHA!!"
            )

        query_dict = {
            "response": h_resp,
            "secret": settings.HSECRET_KEY
        }
        h_req = requests.get(
            "https://hcaptcha.com/siteverify",
            params=query_dict,
        )
        if not h_req:
            raise ValidationError(
                "Captcha server couldn't be reached."
            )

        if not h_req.json().get("success"):
            raise ValidationError(
                h_req.json().get("error-codes")
            )

    def clean_lot_num_1(self):
        lot_num_1 = self.cleaned_data['lot_num_1']
        return lot_num_1.upper()

    def clean_lot_num_2(self):
        lot_num_2 = self.cleaned_data['lot_num_2']
        return lot_num_2.upper()
