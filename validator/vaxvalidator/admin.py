from django.contrib import admin

from .models import JanssenVaccine, ModernaVaccine


@admin.register(JanssenVaccine)
class JanssenVaccineAdmin(admin.ModelAdmin):
    list_display = ("sytem_name", "expiration_date")
    search_fields = ("lot_num",)


@admin.register(ModernaVaccine)
class ModernaVaccineAdmin(admin.ModelAdmin):
    list_display = ("sytem_name", "expiration_date")
    search_fields = ("lot_num",)
