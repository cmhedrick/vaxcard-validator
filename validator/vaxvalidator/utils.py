import json
from typing import List, Dict
from datetime import datetime

import pytz
import requests
from django.conf import settings
from django.utils import timezone
from django.utils.timezone import get_current_timezone

from vaxvalidator.models import JanssenVaccine

local_timezone = get_current_timezone()


def fetch_janssen_lot(lot_code: str):
    user_agent = (
        'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36' +
        ' (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36'
    )
    query_dict = {
        "batch": lot_code,
        "locale": "US",
        "source": "vaxcheck-prod"
    }
    headers = {
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'en-US,en;q=0.9',
        'User-Agent': user_agent,
        'Accept': 'application/json, text/plain, */*',
        'Referer': settings.JNJ_URL,
        "x-api-key": settings.JNJ_API_KEY,
        "Sec-GPC": "1",
        "Pragma": "no-cache",
        "Cache-Control": "no-cache"
    }

    req = requests.get(
        settings.JNJ_URL,
        params=query_dict,
        headers=headers
    )
    return req.json()


def parse_expiration_date(date_str: str):
    parsed_date = local_timezone.localize(
        datetime.strptime(date_str, '%Y-%m-%d')
    )
    return parsed_date


def parse_last_updated_on(date_time_str: str):
    parsed_date = local_timezone.localize(
        datetime.strptime(date_time_str[:-3], '%Y-%m-%d %H:%M:%S.%f')
    )
    return parsed_date


def parse_time_stamp(date_time_str: str):
    cut_str = date_time_str.split("(")[0].strip()
    return datetime.strptime(cut_str, "%a %b %d %Y %H:%M:%S %Z%z")


def create_janssen_vaccine(janssen_json):
    obj, created = JanssenVaccine.objects.update_or_create(
        lot_num=janssen_json["details"].get("batch"),
        expiration_date=parse_expiration_date(
            janssen_json["details"].get("expiration_date")
        ),
        country_code=janssen_json["details"].get("region"),
        product_name=janssen_json["details"].get("productname"),
        last_updated_on=parse_last_updated_on(
            janssen_json["details"].get("lastupdatedon")
        ),
        material=janssen_json["details"].get("material"),
        sku_description=janssen_json["details"].get("skudescription"),
        batch_status=janssen_json["details"].get("batchstatus"),
        unrestricted=janssen_json["details"].get("unrestricted"),
        restricted=janssen_json["details"].get("restricted"),
        quality_inspection=janssen_json["details"].get("qualityinspection"),
        z_enterprise_material_number=janssen_json["details"].get(
            "z_enterprise_material_number"),
        is_negative_temp=janssen_json["details"].get("isNegativeTemp"),
        time_stamp=parse_time_stamp(janssen_json["details"].get("timeStamp")),
    )
    return obj


def get_and_create_jnj(lot_code):
    janssen_json = fetch_janssen_lot(lot_code=lot_code)
    if janssen_json:
        obj = create_janssen_vaccine(janssen_json)
        return obj, True
    else:
        return None, False
